# cesieats_microservices


## Clone

To clone the repository, use the following command:
- git clone --recursive <repository_url>

## Pull

To pull the latest changes from the repository for all submodules, use the following command:
- git submodule update --remote --merge

## Run the microservices

You can easily start all the microservices by running the following command:
- docker-compose up -d --build --scale images=2 --scale users=2 --scale articles=2 --scale orders=2

